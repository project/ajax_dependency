<?php

namespace Drupal\ajax_dependency_example;

use Drupal\ajax_dependency\AjaxDependency;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class AjaxDependencyExampleForm extends FormBase {

  /**
   * { @inheritdoc }
   */
  public function getFormId() {
    return 'ajax_dependency_example_form';
  }

  /**
   * { @inheritdoc }
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['selector'] = [
      '#title' => t('Selector'),
      '#type' => 'checkboxes',
      '#required' => TRUE,
      '#options' => [
        'one' => t('One'),
        'two' => t('Two'),
      ],
      '#default_value' => $form_state->getValue('selector') ?? [],
    ];
    $form['variant_one'] = [
      '#title' => t('Variant one'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $form_state->getValue('variant_one'),
    ];
    $form['variant_two'] = [
      '#title' => t('Variant two'),
      '#type' => 'checkbox',
      '#required' => TRUE,
      '#default_value' => $form_state->getValue('variant_two'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#name' => 'submit',
      '#value' => $this->t('Save'),
    ];

    // As we typically are in aform rebuild withlut validation, form state
    // values are empty, and we must use raw input. As the name suggests, this
    // can be faked arbitrarily and has no csrf protection. As we only toggle
    // form widgets, this is fine.
    // So this does NOT work:
    // AjaxDependency::contentIf($form_state->getValue(['selector', 'one']), $form['selector'], $form['variant_one'], $form_state);
    // AjaxDependency::contentIf($form_state->getValue(['selector', 'two']), $form['selector'], $form['variant_two'], $form_state);
    // @see \Drupal\Core\Form\FormBuilder::handleInputElement
    // @see \Drupal\Core\Form\FormValidator::handleErrorsWithLimitedValidation
    $selectorInput = $form_state->getUserInput()['selector'] ?? NULL;
    AjaxDependency::contentIf($selectorInput['one'] ?? NULL, $form['selector'], $form['variant_one'], $form_state);
    AjaxDependency::contentIf($selectorInput['two'] ?? NULL, $form['selector'], $form['variant_two'], $form_state);

    return $form;
  }

  /**
   * { @inheritdoc }
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

}
