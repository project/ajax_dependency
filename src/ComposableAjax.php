<?php

declare(strict_types=1);
namespace Drupal\ajax_dependency;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\MainContent\MainContentRendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;

final class ComposableAjax {

  /**
   * @param callable(AjaxResponse, array, FormStateInterface): void $processor
   */
  public static function addAjaxResponseProcessor(array &$element, callable $processor): void {
    if (($element['#ajax']['callback'][0] ?? NULL) instanceof self) {
      // Get existing wrapper.
      assert(array_keys($element['#ajax']['callback']) === [0, 1]);
      assert($element['#ajax']['callback'][1] === 'ajaxSubmit');
      $composableAjax = $element['#ajax']['callback'][0];
    }
    else {
      // Create new wrapper.
      $composableAjax = self::create($element['#ajax']['callback'] ?? NULL);
    }
    $composableAjax = $composableAjax->withProcessor($processor);
    $element['#ajax']['callback'] = [
      $composableAjax,
      'ajaxSubmit',
    ];
  }

  /**
   * @param ?callable(array, FormStateInterface): array|AjaxResponse|null $callback
   * @param list<callable(AjaxResponse, array, FormStateInterface): void> $processors
   * */
  private function __construct(
    private readonly string|array|\Closure|null $callback,
    private readonly array $processors,
  ) {}

  private static function create(?callable $callback = NULL): self {
    return new self($callback, []);
  }

  /**
   * @param callable(AjaxResponse, array, FormStateInterface): void $decorator
   */
  private function withProcessor(callable $decorator): self {
    return new self($this->callback, array_merge($this->processors, [$decorator]));
  }

  public function ajaxSubmit(array $form, FormStateInterface $formState): AjaxResponse {
    if (!$this->callback) {
      $response = new AjaxResponse();
    }
    else {
      $result = ($this->callback)($form, $formState);
      /** @see \Drupal\Core\Form\FormAjaxResponseBuilder::buildResponse */
      if ($result instanceof AjaxResponse) {
        $response = $result;
      }
      else {
        assert(is_array($result));
        if (!empty($result['#group'])) {
          unset($result['#group']);
        }
        $response = $this->getAjaxRenderer()->renderResponse($result, $this->getRequest(), $this->getRouteMatch());
        assert($response instanceof AjaxResponse);
      }
    }
    foreach ($this->processors as $processor) {
      $processor($response, $form, $formState);
    }
    return $response;
  }

  private function getAjaxRenderer(): MainContentRendererInterface {
    return \Drupal::service('main_content_renderer.ajax');
  }

  private function getRequest(): Request {
    return \Drupal::request();
  }

  private function getRouteMatch(): RouteMatchInterface {
    return \Drupal::routeMatch();
  }

}
